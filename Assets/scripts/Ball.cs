﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToolScripts;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class CircleType
{
    public float x, y, r;
    public CircleType(float x, float y, float r)
    {
        this.x = x;
        this.y = y;
        this.r = r;
    }
}

public class Ball : MonoBehaviour
{
    
    public  SpriteRenderer ball;
    public Camera camera;
    protected Rect rect;
    public  CircleType player;

    public float speed = 2f;
    public Vector2 directionBall = new Vector2(-2f, 3f);

    ParticleSystem part;
    public bool brickExplose = false;


    // Start is called before the first frame update
    void Start()
    {
        this.tag = "ball";
        //TouchManager.Instance.onTouchMoved += TouchMoved;

         player = new CircleType(ball.transform.position.x, ball.transform.position.y, ball.transform.lossyScale.y / 2);
    }

    private void Update()
    {
        directionBall.Normalize();
        directionBall *= speed;
        
        // bar = brick_breaker_bar.gameObject.GetBoundingBox();// new Rect(brick_breaker_bar.transform.position.x, brick_breaker_bar.transform.position.y, brick_breaker_bar.transform.lossyScale.x, brick_breaker_bar.transform.lossyScale.y);
        Vector2 normal = Vector2.zero;

        if (collisionDetection(player, Bar.instance.bar, ref normal))
        {

            directionBall = crashEdge(directionBall, normal);
            

        }

        //Debug.DrawLine(ball.transform.position, ball.transform.position + (Vector3)directionBall, Color.cyan);

        ball.transform.position += (Vector3)directionBall * Time.deltaTime;
        player = new CircleType(ball.transform.position.x, ball.transform.position.y, ball.transform.lossyScale.y / 2);
    }

    public Vector2 crashEdge(Vector2 directionBall_, Vector2 normal)
    {
        directionBall_.Normalize();
        directionBall_ = Vector2.Reflect(directionBall_, normal);
        return directionBall_;
    }

    public static bool collisionDetection(CircleType circle, Rect rect, ref Vector2 normal)
    {
        float NearestX = Mathf.Clamp(circle.x, rect.x - rect.width / 2, rect.x + rect.width / 2);
        float NearestY = Mathf.Clamp(circle.y, rect.y - rect.height / 2, rect.y + rect.height / 2);

        Vector2 closestPoint = new Vector2(NearestX, NearestY);
        Vector2 circleCenter = new Vector2(circle.x, circle.y);
        Vector2 dp = closestPoint - circleCenter;

        normal = dp.normalized;
        normal *= -1;

        

        float dist = dp.magnitude;
        bool collided = (dist) <= (circle.r);

        /*if (collided)
            Debug.DrawLine((Vector3)closestPoint, (Vector3)circleCenter, Color.red);
            */
        return collided;
    }

    public void normalizeVector(Vector2 _directionBall)
    {
        float length = Mathf.Sqrt((_directionBall.x * _directionBall.x) + (_directionBall.y * _directionBall.y));
        float normalizeX = _directionBall.x / length;
        float normalizeY = _directionBall.y / length;
        Vector2 normalizedDirection = new Vector2(normalizeX, normalizeY);
        //Debug.Log("Nomralized vector:"+normalizedDirection);
        directionBall = normalizedDirection;
    }

    /* public bool collisionDetectionPrize(Rect rect1, Rect rect2)
     {
         //Debug.Log("prize detection!!!!!!!!!!!!!!");
         float rect1Left = rect1.x - rect1.width / 2;
         float rect2Right = rect2.x + rect2.width / 2;
         float rect1Right = rect1.x + rect1.width / 2;
         float rect2Left = rect2.x - rect2.width / 2;
         float rect1Top = rect1.y + rect1.height / 2;
         float rect2Top = rect2.y + rect2.height / 2;
         float rect1Bottom = rect1.y - rect1.height / 2;
         float rect2Bottom = rect2.y - rect2.height / 2; ;


         if (rect1Left < rect2Right && rect1Right > rect2Left && rect1Top < rect2Bottom && rect1Bottom > rect2Top)
         {
             Debug.Log("collision DETECTION PRIZE");
             return true;
         }
         else
         {
             return false;
         }
     }
     */

}