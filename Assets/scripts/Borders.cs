﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Borders : MonoBehaviour
{
    public List<Rect> borders = new List<Rect>(); //sınırlar
                                                  // public static List<GameObject> walls=new List<GameObject>();
    public List<GameObject> walls = new List<GameObject>();
    Vector2 normal = Vector2.zero;
    public Transform gameOverPanel;
   

    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in walls) //sınırlar
        {
            Rect border = item.GetBoundingBox();// new RectType(item.transform.position.x,item.transform.position.y,item.transform.lossyScale.x, item.transform.lossyScale.y);
            borders.Add(border);
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] balls= GameObject.FindGameObjectsWithTag("ball");

        for (int i = 0; i < borders.Count; i++)
        {
            for (int b = 0; b < balls.Length; b++)
            {
                Ball ball = balls[b].GetComponent<Ball>();
                if (Ball.collisionDetection(ball.player, borders[i], ref normal))
                {
                    //brickExplose = true;
                    Debug.Log("collision");
                    if (i == 3)
                    {
                        Debug.Log("bottom'a çarptı");
                        gameOverPanel.gameObject.SetActive(true);
                        Time.timeScale = 0;
                        Time.timeScale = 1;

                       /* if (playAgain.onClick)
                         {
                             SceneManager.LoadScene("brick_breaker");
                           
                        }*/
                         
                       // Debug.Log("game over");
                    }
                    ball.directionBall = ball.crashEdge(ball.directionBall, normal);
                    Debug.Log("DİRECTİON BAAL"+ball.directionBall);
                }
             
            }
           
        }
    }

   /* private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        foreach(var border in borders)
        {
            Gizmos.DrawWireCube(border.position, border.size);
        }
        
    }
    */
}
