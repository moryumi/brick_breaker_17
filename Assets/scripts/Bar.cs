﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToolScripts;

public class Bar : MonoBehaviour
{
    public static Bar instance;
    public  SpriteRenderer brickBreakerBar;
    public  Rect bar;
    public GameObject wallLeft;
    public GameObject wallRight;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //bar = brickBreakerBar.gameObject.GetBoundingBox();// new Rect(brick_breaker_bar.transform.position.x, brick_breaker_bar.transform.position.y, brick_breaker_bar.transform.lossyScale.x, brick_breaker_bar.transform.lossyScale.y);
        TouchManager.Instance.onTouchMoved += TouchMoved;
        UpdateBar();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void UpdateBar()
    {
        bar = brickBreakerBar.gameObject.GetBoundingBox();

    }

    private void TouchMoved(TouchInput touchInput)
    {
        Debug.Log("TOUCH MOVED !!");
        Vector3 position_x = new Vector3(touchInput.DeltaWorldPosition.x, 0, 0);
        float leftWallLlimit = wallLeft.transform.position.x + (wallLeft.transform.position.x / 100);
        Debug.Log("leftwall limit:"+leftWallLlimit);

        float rightWallLimit = 3.31f;
            //wallRight.transform.position.x + (wallRight.transform.position.x / 100);
        Debug.Log("righttwall limit:" + rightWallLimit);

        if ((brickBreakerBar.transform.position.x + (brickBreakerBar.transform.position.x / 2)) >= leftWallLlimit &&
           (brickBreakerBar.transform.position.x + brickBreakerBar.transform.position.x / 2) <= rightWallLimit)
        {


            brickBreakerBar.transform.position += position_x;
            UpdateBar();
        }
        else
        {
            if (brickBreakerBar.transform.position.x > 0)
            {
                brickBreakerBar.transform.position += new Vector3(-0.01f, 0, 0);
            }
            else
            {
                brickBreakerBar.transform.position += new Vector3(0.01f, 0, 0);
            }
        }
    }
}
