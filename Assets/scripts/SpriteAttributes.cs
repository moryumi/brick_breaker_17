﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public static class MyAttributes
{
    public static void FitHeight(this GameObject gameObject, float height)
    {
        if (gameObject.GetComponent<SpriteRenderer>() != null)
        {
            gameObject.transform.localScale = new Vector3(height / gameObject.GetHeightPixel() * gameObject.transform.localScale.x, height / gameObject.GetHeightPixel() * gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        }
    }


    public static void FitWidth(this GameObject gameObject, float width)
    {
        if (gameObject.GetComponent<SpriteRenderer>() != null)
        {
            gameObject.transform.localScale = new Vector3(width / gameObject.GetWidthPixel() * gameObject.transform.localScale.x, width / gameObject.GetWidthPixel() * gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        }
    }


    public static void FitWidthAndHeight(this GameObject gameObject, float width, float height)
    {
        if (gameObject.GetComponent<SpriteRenderer>() != null)
        {
            gameObject.FitWidth(width);
            gameObject.FitHeight(height);
        }
    }


    public static float GetWidthPixel(this GameObject gameObject)
    {
        if (gameObject.GetComponent<SpriteRenderer>() == null) return 0;


        var sprite = gameObject.GetComponent<SpriteRenderer>().sprite;


        return sprite.texture.width * gameObject.transform.lossyScale.x;
    }


    public static float GetHeightPixel(this GameObject gameObject)
    {
        if (gameObject.GetComponent<SpriteRenderer>() == null) return 0;


        var sprite = gameObject.GetComponent<SpriteRenderer>().sprite;


        return sprite.texture.height * gameObject.transform.lossyScale.y;
    }


    public static float GetWidthUnit(this GameObject gameObject)
    {
        if (gameObject.GetComponent<SpriteRenderer>() == null) return 0;


        var sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
        return (sprite.texture.width / sprite.pixelsPerUnit) * gameObject.transform.lossyScale.x;
    }


    public static float GetHeightUnit(this GameObject gameObject)
    {
        if (gameObject.GetComponent<SpriteRenderer>() == null) return 0;


        var sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
        return (sprite.texture.height / sprite.pixelsPerUnit) * gameObject.transform.lossyScale.y;
    }


    public static Rect GetBoundingBox(this GameObject gameObject)
    {
        if (gameObject.GetComponent<SpriteRenderer>() == null) return Rect.zero;


        var offset = gameObject.GetComponent<SpriteRenderer>().sprite.pivot;


        offset = new Vector2(offset.x / gameObject.GetWidthPixel(), offset.y / gameObject.GetHeightPixel());


        var widthUnit = gameObject.GetWidthUnit();
        var heightUnit = gameObject.GetHeightUnit();

        var minX = gameObject.transform.position.x - offset.x * widthUnit;
        var minY = gameObject.transform.position.y - offset.y * heightUnit;

        return new Rect(minX, minY, widthUnit, heightUnit);
    }

   
}

