﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToolScripts;

public class Powerup : MonoBehaviour
{
    // public List<Rect> prizeRectList = new List<Rect>();
    public SpriteRenderer prizeList;
    public Rect prizeRectList;
    public GameObject wallBottom;
    public Vector3 dusur = new Vector3(0, -5f, 0);

    // Start is called before the first frame update
    void Start()
    {
       // UpdatePowerUp();
        //Debug.Log("düşüyoooooo");
    }

    void UpdatePowerUp()
    {
        if (prizeList.transform.position.y >= wallBottom.transform.position.y)
        {
            Debug.Log("update powrup içi");
            Vector3 dusur = new Vector3(0, -5f, 0);
            // Debug.Log("prize List sprite rendere posiiooon" + prizeRectList.position);
            prizeRectList = prizeList.gameObject.GetBoundingBox();
            // Debug.Log("prize List sprite rendere posiiooon"+prizeRectList.position);

            prizeList.transform.position = Bricks.prizeFallPos;
            prizeList.transform.position += dusur * Time.deltaTime * 2.0f;
            Debug.Log("prize List sprite rendere posiiooon INITIAAL" + prizeRectList.position);
            prizeRectList.position += (Vector2)dusur * Time.deltaTime * 2.0f;
            Debug.Log("prize List sprite rendere posiiooon SECONDD" + prizeRectList.position);
        }
       

    }
    // Update is called once per frame
    void Update()
    {
        UpdatePowerUp();
        if (Bricks.isFall)
        {
           
           
        }

        if (collisionDetectionPrize(prizeRectList, Bar.instance.bar))
        {
            Debug.Log("ball & prize collision");
            Debug.Log("prizze'ı destroy et");
        }
    }

    bool collisionDetectionPrize(Rect rect1, Rect rect2)
    {
        //Debug.Log("prize detection!!!!!!!!!!!!!!");
        float rect1Left = rect1.x - rect1.width / 2;
        float rect2Right = rect2.x + rect2.width / 2;
        float rect1Right = rect1.x + rect1.width / 2;
        float rect2Left = rect2.x - rect2.width / 2;
        float rect1Top = rect1.y + rect1.height / 2;
        float rect2Top = rect2.y + rect2.height / 2;
        float rect1Bottom = rect1.y - rect1.height / 2;
        float rect2Bottom = rect2.y - rect2.height / 2; ;

        if (rect1Left < rect2Right && rect1Right > rect2Left && rect1Top < rect2Bottom && rect1Bottom > rect2Top)
        {
            Debug.Log("collision DETECTION PRIZE");
            return true;
        }
        else
        {
            return false;
        }
    }
}