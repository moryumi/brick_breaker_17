﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToolScripts;
using UnityEngine.SceneManagement;

public class Bricks : MonoBehaviour
{
    public List<GameObject> bricks = new List<GameObject>();
    public List<Rect> brick = new List<Rect>();
    public static Vector3 prizeFallPos;

    Powerup powr;

    float timer = 50f;
    Vector2 normal = Vector2.zero;
    Score score;
    public static bool isFall = false;

    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.FindObjectOfType<Score>();
        foreach (var item in bricks)
        {
            Rect brick_break = item.GetBoundingBox();//new Rect(item.transform.position.x,item.transform.position.y, item.transform.lossyScale.x, item.transform.lossyScale.y);
            brick.Add(brick_break);
            // Debug.Log("tuğlalar eklendi");
            Debug.Log(brick);
        }
    }

    // Update is called once per frame
    void Update()
    {

        GameObject[] balls = GameObject.FindGameObjectsWithTag("ball");
        for (int i = 0; i < brick.Count; i++)
        {
            for (int b = 0; b <balls.Length ; b++)
            {
                Ball ball = balls[b].GetComponent<Ball>();
                //print("üst"+direction_ball);
                if (Ball.collisionDetection(ball.player, brick[i], ref normal))
                {
                    //Debug.Log("brick-player collision içinde");

                    ball.directionBall=ball.crashEdge(ball.directionBall, normal);
                    brick.Remove(brick[i]);
                    Debug.Log(bricks[i]);
                    GameObject destroyBrick = bricks[i];

                    prizeFallPos = destroyBrick.transform.position;
                    bricks.Remove(destroyBrick);
                    Destroy(destroyBrick);

                    isFall = true;
                    
                    
                    //score.scoreText.text = " " + score;
                    score.score += 5;
                    
                    if (brick.Count <= 0)
                    {
                        Debug.Log("oyun bitti");

                        while (timer > 0)
                        {
                            timer -= Time.deltaTime;
                            Debug.Log(" " + timer);
                        }

                        if (timer <= 0)
                        {
                            SceneManager.LoadScene("brick_breaker");
                        }
                    }
                }
            }
           
            
        }
    }
}
